<?php

namespace Wings\Apps\Http\Controllers;

use \Wings\Network;

class ConnectionController extends BaseController
{
    public function requestAction()
    {
        $response = $this->response;

        $reply = new Network\Message\WebSocketConnectionResponse();

        $reply
            ->setConnectionHost('ws://192.168.33.11')
            ->setConnectionPort($this->getDi()->getRaw('server')->getServerDi()->getRaw('ws_server')->getPort())
            ->setConnectionVerb('GET')
            ->setConnectionUri('resource/ws/' . $this->getDI()->get('id_generator')->next());


        $response->setBody($reply->getRawData());
        $response->addHeader('Content-Type', 'application/json');

        return $this->response();
    }
}

