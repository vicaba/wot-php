<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 30/04/15
 * Time: 11:41
 */

namespace Wings\Apps\Http\Controllers;

class AssetsController extends BaseController {

    const TYPE_CSS = "CSS";
    const TYPE_JS = "JS";

    protected $assetMap = array(
        "css" => self::TYPE_CSS,
        "js" => self::TYPE_JS
    );

    protected $contentTypeMap = array(
        self::TYPE_CSS => "text/css",
        self::TYPE_JS => "text/javascript"
    );

    public function serveAssetsAction()
    {
        $uri = $this->request->getPath();
        $path = $uri;

        $explodedPath = explode(".", $path);
        $extension = $explodedPath[count($explodedPath) - 1];
        $fileType = $this->assetMap[$extension];

        $logger = $this->getDI()->get("logger");
        $logger->log("Asset Requested on path \"". $path. "\" asset type: ". $fileType);

        // TODO: Security hole here!
        $this->response->setBody(file_get_contents(__DIR__. "/../../". $path));
        $this->response->addHeader("Content-Type", $this->contentTypeMap[$fileType]);
        return $this->response();
    }

}