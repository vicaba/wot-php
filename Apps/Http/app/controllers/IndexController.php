<?php

namespace Wings\Apps\Http\Controllers;


class IndexController extends BaseController
{

    public function indexAction()
    {

        $view = new \Phalcon\Mvc\View();
        $view = $this->view;
        $url = $this->getDI()->get('url');

        $view->setVar('url_request_conn_ws', $string = $url->get(array(
            'for' => 'connection_request',
            'type' => 'ws'
        ))
        );

        $view->setVar("main_header", "Query");

        $view->setVar("partial_js", "index/index_js");

        $view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);
        $view->start();
        $view->render("index", "index"); //Pass a controller/action as parameters if required
        $view->finish();

        $this->response->setStatus(200);
        $this->response->addHeader("Content-Type", "text/html");

        //Set the content of the response
        $this->response->setBody($view->getContent());

        return $this->response();
    }

}

