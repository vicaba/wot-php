<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 27/04/15
 * Time: 14:38
 */

namespace Wings\Apps\Ws\Controllers;

use Phalcon\Mvc\Controller;
use Wings\Network\Message\WebSocketConnectionState;

class ConnectionController extends Controller {

    public function newAction()
    {

        $phalconResponse = new \Phalcon\Http\Response();

        $reply = new WebSocketConnectionState();
        $reply
            ->setConnectionStateCode(200)
            ->setConnectionStateValue("established");

        $phalconResponse->setContent($reply->getRawData());

        return $phalconResponse;
    }
}