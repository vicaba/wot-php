<?php

$di = new \Phalcon\Di\FactoryDefault();
$di->set('router', function () {

    $router = new Phalcon\Mvc\Router();
    $router->setDefaultNamespace('Wings\Apps\Ws\Controllers');
    $router->add(
        "/raw/echo",
        array(
            "controller" => "Raw",
            "action"     => "echo",
        )
    );

    $router->add(
        '/GET/resource/ws/{conn_id:[0-9]+}',
        array(
            'controller' => 'connection',
            'action' => 'new'
        )
    );

    $router->add(
        '/GET/resource/thing/query',
        array(
            'controller' => 'thing',
            'action' => 'query'
        )
    );

    return $router;

});

$di->set('dispatcher', function () {

    $eventsManager = new \Phalcon\Events\Manager();

    $eventsManager->attach('dispatch:beforeException', function ($event, $dispatcher, $exception) {

        switch ($exception->getCode()) {
            case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                echo 'NOT FOUND'. PHP_EOL;
                echo 'FIND me in the dispatcher DI'. PHP_EOL;
                die();
            case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                echo 'FIND me in de dispatcher DI'. PHP_EOL;
                die();
                return false;
        }

    });

    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
    $dispatcher->setDefaultNamespace('Modules\Frontend\Controllers');

    return $dispatcher;

});

$di->set('view', function() {
    $view = new Phalcon\Mvc\View();
    $view->setViewsDir('./Mvc/views/');
    return $view;
});

return $di;