<?php

namespace Wings\Network;

use Phalcon\Di\Injectable;

/**
 * Class AbstractNetworkManager. This class wraps Network messages to events.
 * @package Wings\Network
 */
abstract class Manager extends Injectable
{

}