<?php

namespace Wings\Network;

use Wings\Network\Parser\KwordException;

class Message implements MessageInterface
{

    const MSG_TYPE = 'default';

    const MSG_NAMESPACE = 'default';

    const MSG_TYPE_KWORD = 'type';

    const MSG_SRC_KWORD = 'src';

    private $requiredKeyWords = array(
        self::MSG_TYPE_KWORD => 'type'
    );

    protected $source;

    protected $rawData;

    protected $type;

    protected $namespace;

    protected $built = false;

    public function __construct()
    {
        $this->type = static::MSG_TYPE;

        $this->namespace = static::MSG_NAMESPACE;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }


    public function getSource()
    {
        return $this->source;
    }


    public function setSource($source)
    {
        $this->built = false;
        $this->source = $source;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getRawData()
    {
        if ($this->built == false) {
            $array = $this->buildRawData();
            $this->setRawData(json_encode($array));
        }
        return $this->rawData;
    }

    public function setRawData($rawData)
    {
        $this->built = true;
        $this->rawData = $rawData;
    }

    protected function buildRawData()
    {
        $this->built = true;
        $array = array();

        $source = $this->getSource();
        if (isset($source)) {
            $array[self::MSG_SRC_KWORD] = $source;
        }

        $array[self::MSG_TYPE_KWORD] = $this->type;
        return $array;
    }

    public function buildFromArray($array)
    {
        $this->checkRequiredKeywords($array);

        $class = get_called_class();

        // This is a message
        $self = new $class;

        $self->setType($array[self::MSG_TYPE_KWORD]);

        return $self;

    }

    protected function getRequiredKeywords()
    {
        return $this->requiredKeyWords;
    }

    protected function checkRequiredKeywords($array)
    {
        $requiredKeywords = $this->getRequiredKeywords();

        foreach ($requiredKeywords as $keyword => $semantic) {
            if (!array_key_exists($keyword, $array)) {
                throw new KwordException($keyword, $semantic);
            }
        }
    }

}