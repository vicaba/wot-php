<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 02/06/15
 * Time: 10:29
 */

namespace Wings\Network\Manager;

use React\ZMQ\SocketWrapper;
use Wings\Network\Manager;
use Wings\Network\Message;

/**
 * Class ZMQNetworkManager
 * @package Wings\Network\Manager
 */
class ZMQ extends Manager
{

    /**
     * @var SocketWrapper
     */
    protected $_router;

    public function setRouter(SocketWrapper $router)
    {
        $this->_router = $router;

        $this->registerRouterEvents();
    }

    public function getRouter()
    {
        return $this->_router;
    }

    protected function registerRouterEvents()
    {
        $this->_router->on('messages', function ($msg) {
            $this->onMessages($msg);
        });

        $this->_router->on('message', function ($msg) {
            $this->onMessage($msg);
        });

        $this->_router->on('error', function ($msg) {
            $this->onError($msg);
        });


    }

    protected function onMessage($msg)
    {

        $logger = $this->getDI()->get("logger");

    }

    protected function onMessages($msg)
    {

        $logger = $this->getDI()->get("logger");

        $data = $msg[1];

        $parser = $this->getDI()->get('message_parser');

        $message = $parser->parseFromJsonString($data);

        $this->getEventsManager()->fire('message:'. $message->getNamespace(), null, $data);

        ob_start();
        var_dump($msg);
        $result = ob_get_clean();

        $logger->log("ZMQNetworkManager: onMessage: " . $result);

        echo 'Router message'. PHP_EOL;
        var_dump($data);

    }

    protected function onError($msg)
    {
        $logger = $this->getDI()->get("logger");

        ob_start();
        var_dump($msg);
        $result = ob_get_clean();

        $logger->log("ZMQNetworkManager: onError: " . $result);

        var_dump($msg);

    }

}