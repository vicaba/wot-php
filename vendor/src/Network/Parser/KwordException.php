<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 02/06/15
 * Time: 13:03
 */

namespace Wings\Network\Parser;


class KwordException extends Exception
{

    public function __construct($keyword, $semantic = "undefined")
    {
        parent::__construct("Message $keyword field required, meaning: $semantic.");
    }

}