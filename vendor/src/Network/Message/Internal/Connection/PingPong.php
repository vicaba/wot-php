<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 08/06/15
 * Time: 11:05
 */

namespace Wings\Network\Message\Internal\Connection;

use Wings\Network\Message\Internal\Internal;

abstract class PingPong extends Internal
{

    private $requiredKeywords = array(
        self::MSG_SRC_KWORD => 'source'
    );

    public function buildFromArray($array)
    {

        $this->setSource($array[self::MSG_SRC_KWORD]);

        $self = parent::buildFromArray($array);

        return $self;
    }

    protected function getRequiredKeywords()
    {
        $array = parent::getRequiredKeywords();

        return array_merge($array, $this->requiredKeywords);
    }

}