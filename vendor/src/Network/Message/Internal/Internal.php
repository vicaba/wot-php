<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 08/06/15
 * Time: 11:21
 */

namespace Wings\Network\Message\Internal;


use Wings\Network\Message;

class Internal extends Message
{

    const MSG_NAMESPACE = 'internal';

    const MSG_VERB_KWORD = 'verb';

    const MSG_URI_KWORD = 'uri';

    protected $verb;

    protected $uri;

    private $requiredKeywords = array(
        self::MSG_VERB_KWORD => 'source',
        self::MSG_URI_KWORD => 'uri'
    );

    /**
     * @return mixed
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * @param mixed $verb
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function buildFromArray($array)
    {

        $this->setVerb($array[static::MSG_VERB_KWORD]);
        $this->setUri($array[static::MSG_URI_KWORD]);

        $self = parent::buildFromArray($array);

        return $self;
    }

    protected function getRequiredKeywords()
    {
        $array = parent::getRequiredKeywords();

        return array_merge($array, $this->requiredKeywords);
    }

    protected function buildRawData()
    {
        $array = parent::buildRawData();

        $array[static::MSG_VERB_KWORD] = $this->getVerb();
        $array[static::MSG_URI_KWORD] = $this->getUri();

        return $array;

    }

}