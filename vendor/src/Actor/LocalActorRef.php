<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 22/06/15
 * Time: 11:00
 */

namespace Wings\Actor;


class LocalActorRef
    implements ActorRef
{

    protected $localAddress;

    protected $localRouter;

    protected $receiveCallback = null;

    public function getLocalAddress()
    {
        return $this->localAddress;
    }

    public function setLocalAddress($localAddress)
    {
        $this->localAddress = $localAddress;
    }

    public function getLocalRouter()
    {
        return $this->localRouter;
    }

    public function setLocalRouter($localRouter)
    {
        $this->localRouter = $localRouter;
    }

    protected function sendTo($to, $message)
    {

    }

    protected function receive($sender, $message)
    {

    }

    public function onReceive(callable $receiveCallback)
    {
        $this->receiveCallback = $receiveCallback;
    }

    public function detachReceiveCallback()
    {
        $this->receiveCallback = null;
    }

}