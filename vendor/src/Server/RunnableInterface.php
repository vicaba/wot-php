<?php

namespace Wings\Server;

interface RunnableInterface {

    public function run();
    
}