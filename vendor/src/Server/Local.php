<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 10/03/15
 * Time: 09:59
 */

namespace Wings\Server;

use Wings\InitializableInterface;

abstract class Local
    extends AbstractServer
    implements InitializableInterface, RunnableInterface

{

    protected $_loop;

    /**
     * @var \Ratchet\Server\IoServer
     */
    protected $_server;

    /**
     * @var \Phalcon\DiInterface
     */
    protected $_serverDi;

    /**
     * @var \Phalcon\DiInterface
     */
    protected $_internalDi;

    /**
     * @var \Ratchet\MessageComponentInterface
     */
    protected $_connectionHandler;

    protected $_application;

    abstract protected function createServer();

    /**
     * @return mixed
     */
    public function getLoop()
    {
        return $this->_loop;
    }

    /**
     * @param $_loop
     * @return $this
     */
    public function setLoop(\React\EventLoop\LoopInterface $_loop)
    {
        $this->_loop = $_loop;
        return $this;
    }

    public function setServerDi(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->_serverDi = $dependencyInjector;
        return $this;
    }

    /**
     * @return \Phalcon\DiInterface
     */
    public function getServerDi()
    {
        return $this->_serverDi;
    }

    public function setInternalDi(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->_internalDi = $dependencyInjector;
        return $this;
    }

    /**
     * @return \Phalcon\DiInterface
     */
    public function getInternalDi()
    {
        return $this->_internalDi;
    }

    /**
     * @return mixed
     */
    public function getConnectionHandler()
    {
        return $this->_connectionHandler;
    }

    /**
     * @param $_connectionHandler
     * @return $this
     */
    public function setConnectionHandler(\Ratchet\MessageComponentInterface $_connectionHandler)
    {
        $this->_connectionHandler = $_connectionHandler;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplication()
    {
        return $this->_application;
    }

    /**
     * @param $application
     * @return $this
     */
    public function setApplication(\Phalcon\Mvc\Application $application)
    {
        $this->_application = $application;
        return $this;
    }



}