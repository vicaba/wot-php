<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 09/06/15
 * Time: 10:00
 */

namespace Wings\Server;

abstract class AbstractServer
    implements ServerInterface
{

    protected $address;

    protected $port;

    protected $networkManager;

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    public function getPort()
    {
       return $this->port;
    }

    public function setNetworkManager(\Wings\Network\Manager $networkManager)
    {
        $this->networkManager = $networkManager;
        return $this;
    }

    public function getNetworkManager()
    {
        return $this->networkManager;
    }

    public function tell(\Wings\Network\MessageInterface $message)
    {

    }


}