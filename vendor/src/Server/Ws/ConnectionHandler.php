<?php
namespace Wings\Server\Ws;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Phalcon;

class ConnectionHandler implements MessageComponentInterface, Phalcon\Di\InjectionAwareInterface
{

    protected $_di;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo 'Client connected on WS server!';

        $application = $this->_di->getRaw('application');
        $application->getDI()->set('connection', $conn);
        $application->getDI()->set('connections', $this->clients);

        $content = $application->handle($conn->WebSocket->request->getPath())->getContent();
        echo 'Client connection handled!';

        $conn->send($content);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $jsonArray = json_decode($msg, true);
        $application = $this->_di->getRaw('application');
        $application->getDI()->set('connection', $from);
        $application->getDI()->set('data', $jsonArray["query"]);

        var_dump($jsonArray["query"]);

        $content = $application->handle($jsonArray["uri"])->getContent();

        $from->send($content);

    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function setDi(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->_di = $dependencyInjector;
    }

    /**
     * @return \Phalcon\DiInterface
     */
    public function getDI()
    {
        return $this->_di;
    }
}