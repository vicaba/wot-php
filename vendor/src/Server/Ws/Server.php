<?php

namespace Wings\Server\Ws;

use Ratchet\Server\IoServer as RatchetServer;
use Ratchet\Http\HttpServer as HttpServer;
use Ratchet\WebSocket\WsServer as WsServer;
use Ratchet;
use React;
use Phalcon;

use Wings;

class Server
    extends Wings\Server\Local
{
    /**
     * Bootstraps the server.
     */
    public function initialize()
    {

        $this->getApplication()->getDI()->set('server', $this, true);
        $this->createServer();

    }

    protected function createServer()
    {
        $address = $this->getAddress();
        $port = $this->getPort();
        $loop = $this->getLoop();

        $socket = new React\Socket\Server($loop);
        $socket->listen($port, $address);

        $this->getConnectionHandler()->getDi()->set('application', $this->getApplication(), true);

        $this->_server = new RatchetServer(
            new HttpServer(
                new WsServer(
                    $this->getConnectionHandler()
                )
            ),
            $socket, $loop
        );
    }

    /**
     * Run the server in a loop
     */
    public function run()
    {

        $this->_server->run();
    }



}