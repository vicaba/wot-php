<?php

namespace Wings\Server\Http;

use Guzzle\Http\Message\RequestInterface;
use Ratchet\Http\HttpServerInterface;
use Ratchet\ConnectionInterface;
use Phalcon;

class ConnectionHandler
    implements HttpServerInterface, Phalcon\Di\InjectionAwareInterface
{
    /**
     * @var \Phalcon\Di
     */
    protected $_di;

    public function __construct()
    {
    }

    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null)
    {

        // Get the application
        $application = $this->_di->getRaw('application');

        $appDi = $application->getDi();

        // Inject the request
        $appDi->set('request', function () use ($request) {
            return $request;
        });

        // Inject a new response
        $appDi->set('response', function () {
            return new \Guzzle\Http\Message\Response(200);
        });

        // Handle the URI
        $content = $application->handle($request->getPath())->getContent();

        $conn->send($content);
        $conn->close();
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        echo 'Http client disconnected!';
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function setDi(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->_di = $dependencyInjector;
    }

    /**
     * @return \Phalcon\DiInterface
     */
    public function getDI()
    {
        return $this->_di;
    }
}