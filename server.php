<?php

namespace Wings;

use Phalcon\Di;
use Wings;
use Ratchet;
use React;
use malkusch\autoloader\Autoloader;

require __DIR__ . '/../../php-autoloader-1.14.4/autoloader.php';

require __DIR__ . '/../../include/vendor/autoload.php';

error_reporting(E_ALL);

// Use the PHP autoloader only for importing own files (Wing)
$autoloader = new Autoloader(__DIR__);
$autoloader->register();

$loop = React\EventLoop\Factory::create();

$serverDi = new Di();

$wsServer = new Wings\Server\Ws\Server();
$wsServer->setAddress("0.0.0.0")
    ->setPort("8081")
    ->setLoop($loop)
    ->setServerDi($serverDi)
    ->setInternalDi(new Di())
->setApplication((new Wings\Apps\Ws\Bootstrap())->getApplication());

$httpServer = new Wings\Server\Http\Server();
$httpServer->setAddress("0.0.0.0")
    ->setPort("8080")
    ->setLoop($loop)
    ->setServerDi($serverDi)
    ->setInternalDi(new Di())
    ->setApplication((new Wings\Apps\Http\Bootstrap())->getApplication());

$httpConnectionHandler = new Wings\Server\Http\ConnectionHandler();
$httpConnectionHandler->setDi(new Di());

$wsConnectionHandler = new Wings\Server\Ws\ConnectionHandler();
$wsConnectionHandler->setDi(new Di());

$httpServer->setConnectionHandler($httpConnectionHandler);
$wsServer->setConnectionHandler($wsConnectionHandler);

$httpServer->getServerDi()->set('http_server', $httpServer, true);
$wsServer->getServerDi()->set('ws_server', $wsServer, true);

$wsServer->initialize();
$httpServer->initialize();

$loop->run();
